function checkInput(event) {
    const numbersOnly = /^\d+$/;
    if (!numbersOnly.test(event.data)) event.target.value = event.target.value.replace(event.data, "");
}

function mask() {
    if (!validate()) return;
    action.innerText = "Masked Card Number: ";
    const parts = breakNumbers();
    output.innerText = `${parts[0]} **** **** ${parts[3]}`;
}

function unmask() {
    if (!validate()) return;
    action.innerText = "Unmasked Card Number: ";
    const parts = breakNumbers();
    output.innerText = `${parts[0]} ${parts[1]} ${parts[2]} ${parts[3]}`;
}

function validate() {
    const length = creditCardNumberInput.value.length;

    if (length > 16) alert("Card number should not exceed 16 digits.");
    if (length < 16) alert("Card number should be 16 digits long.");
    return length === 16 ? true : false;
}

function breakNumbers() {
    const part1 = creditCardNumberInput.value.slice(0, 4);
    const part2 = creditCardNumberInput.value.slice(4, 8);
    const part3 = creditCardNumberInput.value.slice(8, 12);
    const part4 = creditCardNumberInput.value.slice(12, 16);
    return [part1, part2, part3, part4];
}

// event listeners
const creditCardNumberInput = document.getElementById("credit-card-number");
const maskBtn = document.getElementById("mask");
const unmaskBtn = document.getElementById("unmask");

creditCardNumberInput.addEventListener("input", checkInput);
maskBtn.addEventListener("click", mask);
unmaskBtn.addEventListener("click", unmask);
