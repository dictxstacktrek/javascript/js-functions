const recipes = [];
const recipeName = document.getElementById("recipe-name");
const listRecipes = document.querySelector(".recipes");
const recipeInput = document.querySelector(".recipe-input");
const recipeDetails = document.querySelector(".recipe-details");
const listIngredients = document.querySelector(".ingredients");
const listInstructions = document.querySelector(".instructions");
const addIngredientBtn = document.getElementById("add-ingredient");
const addInstructionBtn = document.getElementById("add-instruction");
const saveRecipeBtn = document.getElementById("save");
const newRecipeBtn = document.getElementById("new");
const viewRecipesBtn = document.getElementById("view");

// initial
recipeDetails.style.display = "none";

// event listeners
addIngredientBtn.addEventListener("click", addIngredient);
addInstructionBtn.addEventListener("click", addInstruction);
saveRecipeBtn.addEventListener("click", saveRecipe);
newRecipeBtn.addEventListener("click", newRecipe);
viewRecipesBtn.addEventListener("click", viewRecipes);

function saveRecipe() {
    if (recipeName.value.trim() === "") return;

    const newRecipe = {};
    newRecipe.name = recipeName.value.trim();
    newRecipe.ingredients = Array.from(document.querySelectorAll(".ingredients li"));
    newRecipe.instructions = Array.from(document.querySelectorAll(".instructions li"));
    recipes.push(newRecipe);

    const li = document.createElement("li");
    li.innerText = newRecipe.name;
    li.addEventListener("click", showRecipe);
    listRecipes.appendChild(li);
    clearInputs();
    clearList(listIngredients);
    clearList(listInstructions);
    viewRecipes();
}

function showRecipe() {
    const recipe = recipes.find((r) => r.name === this.innerText);
    document.querySelector(".recipe-details h3").innerText = recipe.name;
    renderList(listIngredients, recipe.ingredients);
    renderList(listInstructions, recipe.instructions);
}

function newRecipe() {
    saveRecipeBtn.style.display = "inline";
    recipeDetails.style.display = "none";
    recipeInput.style.display = "block";

    hideButtons(false);
    hideInputs(false);
    clearList(listIngredients);
    clearList(listInstructions);
}

function viewRecipes() {
    saveRecipeBtn.style.display = "none";
    recipeDetails.style.display = "block";
    recipeInput.style.display = "none";

    hideButtons(true);
    hideInputs(true);
}

function addIngredient() {
    const ingredient = document.getElementById("ingredient");
    const li = document.createElement("li");

    li.innerText = ingredient.value;
    ingredient.value = "";
    listIngredients.appendChild(li);
}

function addInstruction() {
    const instruction = document.getElementById("instruction");
    const li = document.createElement("li");

    li.innerText = instruction.value;
    instruction.value = "";
    listInstructions.appendChild(li);
}

// helper functions
function clearInputs() {
    document.querySelectorAll("main input").forEach((input) => (input.value = ""));
}

function hideButtons(hide) {
    document.querySelectorAll("main button").forEach((btn) => (btn.style.display = hide ? "none" : "inline"));
}

function hideInputs(hide) {
    document.querySelectorAll("main input").forEach((input) => (input.style.display = hide ? "none" : "inline"));
}

function renderList(listName, arrayList) {
    clearList(listName);
    arrayList.forEach((list) => listName.appendChild(list));
}

function clearList(listName) {
    listName.innerHTML = "";
}
