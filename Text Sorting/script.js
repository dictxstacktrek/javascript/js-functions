function addList() {
    const inputValue = input.value;
    if (inputValue.trim() === "") return;
    const p = document.createElement("p");
    p.innerText = inputValue;
    display.appendChild(p);
    input.value = "";
}

function sortList() {
    const lists = Array.from(document.querySelectorAll("#display p"));
    display.innerHTML = "";
    lists.sort((item, NextItem) => item.innerText.localeCompare(NextItem.innerText));

    lists.forEach((item) => {
        display.appendChild(item);
    });
}

function unsortList() {
    const lists = Array.from(document.querySelectorAll("#display p"));
    display.innerHTML = "";
    lists.sort((item, NextItem) => item.innerText.localeCompare(NextItem.innerText));

    lists.reverse().forEach((item) => {
        display.appendChild(item);
    });
}

//event listeners
const addBtn = document.getElementById("add-btn");
const sortBtn = document.getElementById("sort-btn");
const unsortBtn = document.getElementById("unsort-btn");

addBtn.addEventListener("click", addList);
sortBtn.addEventListener("click", sortList);
unsortBtn.addEventListener("click", unsortList);
